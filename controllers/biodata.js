const UserBiodata = require("../models").user_game_biodatas;
const User = require("../models").user_game;
const { validationResult } = require("express-validator");

module.exports = {
  getBiodata: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.userId;

    const biodata = await UserBiodata.findOne({
      where: { user_id: userId },
      include: [{ model: User }],
    });

    res.status(200).json({
      biodata: biodata,
    });
  },

  createBiodata: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.userId;
    const { first_name, last_name, country, email } = req.body;

    const biodata = await UserBiodata.findOne({ where: { user_id: userId } });

    if (biodata) {
      return res.status(500).json({
        message: "User biodata already exists",
      });
    }

    const user = await User.findByPk(userId);

    if (!user) {
      return res.status(404).json({
        message: "User not found",
      });
    }

    let newBiodata;

    try {
      newBiodata = await UserBiodata.create({
        user_id: userId,
        first_name: first_name,
        last_name: last_name,
        country: country,
        email: email,
      });
    } catch (error) {
      return res.status(500).json({
        error: error.parent.detail || error,
      });
    }

    res.status(200).json({
      biodata: newBiodata,
    });
  },

  updateBiodata: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.userId;
    const { first_name, last_name, country, email } = req.body;

    const biodata = await UserBiodata.findOne({ where: { user_id: userId } });
    if (!biodata) {
      return res.status(404).json({
        message: "User biodata not found",
      });
    }

    const updatedBiodata = await UserBiodata.update(
      {
        user_id: userId,
        first_name: first_name,
        last_name: last_name,
        country: country,
        email: email,
      },
      {
        where: { user_id: userId },
        returning: true,
      }
    );

    res.status(200).json({
      message: "User biodata successfully updated",
      beforeUpdate: biodata,
      afterUpdate: updatedBiodata[1][0],
    });
  },

  deleteBiodata: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.userId;

    const biodata = await UserBiodata.findOne({ where: { user_id: userId } });
    if (!biodata) {
      return res.status(404).json({
        message: "User biodata not found",
      });
    }

    await UserBiodata.destroy({
      where: {
        user_id: userId,
      },
    });

    res.status(200).json({
      message: "User biodata successfully deleted",
      deletedBiodata: biodata,
    });
  },
};
