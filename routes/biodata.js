const router = require("express").Router();
const { param, body } = require("express-validator");
const biodataController = require("../controllers/biodata");

router.get(
  "/:userId",
  param("userId").notEmpty().isInt().withMessage("Valid User ID is required"),
  biodataController.getBiodata
);
router.post(
  "/:userId",
  [
    param("userId").notEmpty().isInt().withMessage("Valid User ID is required"),
    body("email")
      .isEmail()
      .withMessage("Email must be valid")
      .optional({ nullable: true, checkFalsy: true }),
  ],
  biodataController.createBiodata
);
router.patch(
  "/:userId",
  param("userId").notEmpty().isInt().withMessage("Valid User ID is required"),
  body("email")
    .isEmail()
    .withMessage("Email must be valid")
    .optional({ nullable: true, checkFalsy: true }),
  biodataController.updateBiodata
);
router.delete(
  "/:userId",
  param("userId").notEmpty().isInt().withMessage("Valid User ID is required"),
  biodataController.deleteBiodata
);

module.exports = router;
