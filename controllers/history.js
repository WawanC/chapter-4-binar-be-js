const UserHistory = require("../models").user_game_histories;
const User = require("../models").user_game;
const { validationResult } = require("express-validator");

module.exports = {
  getHistories: async (req, res) => {
    const games = await UserHistory.findAll({
      include: [{ model: User }],
    });
    res.status(200).json({
      games: games,
    });
  },

  getHistory: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        errors: errors.array(),
      });
    }

    const userId = req.params.userId;

    const games = await UserHistory.findAll({
      where: { user_id: userId },
      include: [{ model: User }],
    });

    res.status(200).json({
      games: games,
    });
  },

  createHistory: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        errors: errors.array(),
      });
    }

    const userId = req.params.userId;
    const { title, publisher, score, hours_time } = req.body;

    let newGame;
    try {
      newGame = await UserHistory.create({
        user_id: userId,
        title: title,
        publisher: publisher,
        score: score,
        hours_time: hours_time,
      });
    } catch (error) {
      return res.status(500).json({
        error: error.parent.detail || error,
      });
    }

    res.status(200).json({
      game: newGame,
    });
  },

  updateHistory: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        errors: errors.array(),
      });
    }

    const userId = req.params.userId;
    const gameId = req.query.gameId;

    const game = await UserHistory.findByPk(gameId);
    if (!game) {
      return res.status(404).json({
        message: "Game history is not found",
      });
    }

    const { title, publisher, score, hours_time } = req.body;

    const updatedGame = await UserHistory.update(
      {
        title: title,
        publisher: publisher,
        score: score,
        hours_time: hours_time,
      },
      {
        where: { id: gameId, user_id: userId },
        returning: true,
      }
    );

    res.status(200).json({
      message: "User games history successfully updated",
      beforeUpdate: game,
      afterUpdate: updatedGame[1][0],
    });
  },

  deleteHistory: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        errors: errors.array(),
      });
    }

    const userId = req.params.userId;
    const gameId = req.query.gameId;

    const game = await UserHistory.findByPk(gameId);
    if (!game) {
      return res.status(404).json({
        message: "Game history is not found",
      });
    }

    await UserHistory.destroy({
      where: {
        id: gameId,
        user_id: userId,
      },
    });

    res.status(200).json({
      message: "User game history successfully deleted",
      beforeDeleted: game,
    });
  },
};
