const express = require("express");
const userRoutes = require("./routes/user");
const biodataRoutes = require("./routes/biodata");
const historyRoutes = require("./routes/history");

const app = express();

app.use(express.json());

app.use("/user", userRoutes);
app.use("/biodata", biodataRoutes);
app.use("/game", historyRoutes);

app.listen(8000, () => {
  console.log("Server listening on 8000");
});
