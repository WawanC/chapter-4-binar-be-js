const UserHistory = require("../models").user_game_histories;
const UserBiodata = require("../models").user_game_biodatas;
const { validationResult } = require("express-validator");

const User = require("../models").user_game;

module.exports = {
  getUser: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.id;
    const user = await User.findByPk(userId, {
      include: [{ model: UserBiodata }, { model: UserHistory }],
    });
    res.status(200).json({
      user: user,
    });
  },

  getUsers: async (req, res) => {
    const users = await User.findAll();
    res.status(200).json({
      users: users,
    });
  },

  createUser: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const { username, password } = req.body;

    const newUser = await User.create({ username, password });

    res.status(200).json({
      user: newUser,
    });
  },

  updateUser: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.id;
    const { username, password } = req.body;

    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).json({
        message: "User not found",
      });
    }

    const updatedUser = await User.update(
      {
        username: username,
        password: password,
      },
      {
        where: {
          id: userId,
        },
        returning: true,
      }
    );

    res.status(200).json({
      message: "User successfully updated",
      beforeUpdate: user,
      afterUpdate: updatedUser[1][0],
    });
  },

  deleteUser: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.id;

    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).json({
        message: "User not found",
      });
    }

    await User.destroy({
      where: {
        id: userId,
      },
    });

    res.status(200).json({
      message: "User successfully deleted",
      deletedUser: user,
    });
  },
};
