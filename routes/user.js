const router = require("express").Router();
const { param, body } = require("express-validator");
const userController = require("../controllers/user");

router.get("/", userController.getUsers);
router.get(
  "/:id",
  param("id").notEmpty().isInt().withMessage("Valid User ID is required"),
  userController.getUser
);
router.post(
  "/",
  body(["username", "password"])
    .notEmpty()
    .withMessage("Username and Password is required"),
  userController.createUser
);
router.patch(
  "/:id",
  param("id").notEmpty().isInt().withMessage("Valid User ID is required"),
  userController.updateUser
);
router.delete(
  "/:id",
  param("id").notEmpty().isInt().withMessage("Valid User ID is required"),
  userController.deleteUser
);

module.exports = router;
